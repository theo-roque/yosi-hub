import React, { useState } from "react";
import styled from "styled-components";
import Navigation from "../../components/Navigation";
import Map from "../../components/Map";
import Modal from "../../components/Modal";

const Main = styled.main`
    display: flex;
    flex-flow: row nowrap;
    height: 100vh;
    width: 100vw;
    position: relative;

    @media (max-width: 850px) {
        flex-flow: column nowrap;
        height: 96vh;
    }
`;

const AnnouncementWrapper = styled.div`
    display: flex;
    flex-flow: column nowrap;
    justify-content: center;
    align-items: center;
    padding: 16px;
`;

const App = () => {
    const [open, setOpen] = useState(true);
    return (
        <Main>
            {open && (
                <Modal onClose={() => setOpen(false)}>
                    <AnnouncementWrapper>
                        <h2>Welcome to Yosi Hub!</h2>
                        <p>
                            We are a community-based application dedicated to
                            help smokers and vapers find a legal smoking area
                            throughout the country.
                        </p>
                        <p>
                            Share the markers with friends and
                            co-smokers/vapers!
                        </p>
                        <p>
                            <div>
                                <img
                                    src="/img/marker-confirmed.svg"
                                    title="Confirmed Smoking Area"
                                    alt="Confirmed Smoking Area"
                                    height="60px"
                                />
                                <p>Confirmed Smoking Area</p>
                            </div>
                            <br />
                            <div>
                                <img
                                    src="/img/marker-pending.svg"
                                    title="Pending Smoking Area"
                                    alt="Pending Smoking Area"
                                    height="60px"
                                />
                                <p>Pending Smoking Area</p>
                            </div>
                        </p>
                    </AnnouncementWrapper>
                </Modal>
            )}
            <Navigation />
            <Map />
        </Main>
    );
};

export default App;
