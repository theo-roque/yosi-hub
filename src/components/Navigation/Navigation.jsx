import React, { useState, useRef, useEffect } from "react";
import { useDispatch } from "react-redux";
import styled from "styled-components";
import { usePlaceSuggestions } from "../../hooks/usePlaceSuggestions";

/*
 * Styles
 */
const Container = styled.div`
    width: 25%;
    height: 100%;
    display: flex;
    flex-flow: column nowrap;
    align-items: flex-end;
    justify-content: flex-start;
    position: absolute;
    right: 0;

    @media (max-width: 850px) {
        width: 100%;
    }
`;

// const Logo = styled.div`
//     font-size: 1rem;
//     color: ${props => props.theme.whiteColor};
//     text-shadow: ${props => props.theme.textShadow};
//     z-index: 9999;
//     -webkit-text-stroke-color: ${props => props.theme.grayColor};
//     -webkit-text-stroke-width: 0.25px;

//     @media (max-width: 850px) {
//         font-size: 16px;
//     }
// `;

const SearchForm = styled.form`
    display: flex;
    flex-flow: row nowrap;
    justify-content: flex-end;
    width: 100%;
    z-index: 999;
    padding: 8px;
    width: -webkit-fill-available;
`;

const SearchBox = styled.input`
    font-size: 14px;
    padding: 12px;
    border: none;
    width: 100%;
    border-radius: 8px 0 0 8px;
    border-right: 10px solid ${props => props.theme.lightGrayColor};
    border-left: 10px solid ${props => props.theme.redColor};
    box-shadow: ${props => props.theme.boxShadow};
    color: ${props => props.theme.blackColor};
    flex: 1;
    position: relative;
    transition: ease-in-out 1s;

    @media (max-width: 850px) {
        font-size: 14px;
    }

    &:focus,
    &:hover,
    &:active {
        outline: none;
        border-left: 20px solid #ff2400;
    }
`;

const Suggestion = styled.div`
    cursor: pointer;
    font-size: 18px;
    border-radius: 8px;
    background: ${props => props.theme.whiteColor};
    color: ${props => props.theme.blackColor};
    box-shadow: ${props => props.theme.boxShadow};
    padding: 0;
    flex-basis: 100%;
    z-index: 9999;
    overflow: auto;
    margin-top: 4px;
    margin-right: 8px;
    margin-bottom: 16px;
    width: 100%;

    @media (max-width: 850px) {
        font-size: 14px;
    }
`;

const SuggestionItem = styled.div`
    font-family: "Roboto";
    background: ${props => props.theme.whiteColor};
    padding: 8px;
    line-height: 1.5rem;

    &:hover {
        background: ${props => props.theme.lightGrayColor};
    }
`;

const SearchEnd = styled.div`
    width: 100%;
    max-width: 80px;
    background: ${props => props.theme.brownColor};
    box-shadow: ${props => props.theme.boxShadow};
    border: none;
    border-radius: 0 8px 8px 0;

    @media (max-width: 850px) {
        font-size: 18px;
    }
`;

// const ImgSmoke = styled.img`
//     height: 200px;
//     position: absolute;
//     left: 50px;
//     top: 2%;
//     z-index: 1;
//     opacity: 0.35;

//     @media (max-width: 850px) {
//         top: -30%;
//         left: 0;
//     }
// `;

/*
 * Navigation Component
 */
const Navigation = () => {
    const [input, setInput] = useState("");
    const [showSuggestions, setShowSuggestions] = useState(false);
    const suggestions = usePlaceSuggestions(input);
    const searchRef = useRef(null);
    const dispatch = useDispatch();

    const handleSuggestionClick = suggestion => {
        const { name } = suggestion;
        setShowSuggestions(false);
        setInput(name);
        dispatch({ type: "SUBMIT_PLACE", response: suggestion });
    };

    useEffect(() => {
        searchRef.current.focus();
    }, []);

    return (
        <Container>
            {/* <ImgSmoke src="/img/smoke.png" alt="smoke" /> */}
            {/* <Logo>
                <h1>Yosi Hub</h1>
            </Logo> */}
            <SearchForm onSubmit={e => e.preventDefault()}>
                <SearchBox
                    value={input}
                    onChange={event => {
                        setShowSuggestions(true);
                        setInput(event.target.value);
                    }}
                    placeholder="Find a smoking area here"
                    ref={searchRef}
                />
                <SearchEnd />
            </SearchForm>
            {showSuggestions && suggestions.length > 0 && (
                <Suggestion>
                    {suggestions.map((suggestion, index) => (
                        <SuggestionItem
                            key={index}
                            onClick={() => handleSuggestionClick(suggestion)}
                        >
                            {suggestion.name}
                        </SuggestionItem>
                    ))}
                </Suggestion>
            )}
        </Container>
    );
};

export default Navigation;
