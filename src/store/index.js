import { createStore } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import combinedReducers from "../reducers";

/**
|--------------------------------------------------
| Put reducers to local storage
| Do not store form to local storage
|--------------------------------------------------
*/

/**
|--------------------------------------------------
| Put reducers to local storage
| Do not store form to local storage
|--------------------------------------------------
*/
const reducer = persistReducer({ key: "state", storage }, combinedReducers);

//  Prepare store for Provider
const store = createStore(reducer);

//  Prepare persistor for persistStore
const persistor = persistStore(store);

//  Exporting store and persistor for providers in index
export { store, persistor };
