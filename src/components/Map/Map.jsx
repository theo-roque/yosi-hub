import React, { useState, useEffect, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import uuid from "react-uuid";
import styled from "styled-components";
import { MdPinDrop, MdPlace } from "react-icons/md";
import mapboxgl from "mapbox-gl";
import { usePosition } from "../../hooks/usePosition";
import Modal from "../../components/Modal";
import Input from "../../components/Input";
import Button from "../../components/Button";
import "./Map.css";

const Container = styled.div`
    flex: 1;
    background: #fff;
    position: relative;

    @media (max-width: 850px) {
        flex: 2;
        order: 1;
    }

    .marker {
        background-image: url("/img/marker-pending.svg");
        background-size: cover;
        width: 60px;
        height: 60px;
        cursor: pointer;
    }
`;

const MapLoader = styled.div`
    flex: 1;
    position: relative;
    width: 100%;
    height: 100%;
    background: white;
`;

const MapArea = styled.div`
    position: absolute;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    height: 100%;
    width: 100%;

    .leaflet-control-zoom {
        margin-bottom: 80px;
    }
`;

const MapBtnsWrapper = styled.div`
    display: flex;
    flex-flow: column nowrap;
    position: absolute;
    margin: 8px 0;
    right: 0;
    bottom: 50%;

    > button:not(:first-child) {
        margin-top: 8px;
    }
`;

const MapBtn = styled.button`
    font-size: 22px;
    padding: 8px;
    height: 40px;
    width: 40px;
    color: ${props => props.theme.redColor};
    background: ${props => props.theme.whiteColor};
    box-shadow: ${props => props.theme.boxShadow};
    cursor: pointer;
    z-index: 999;
    border: none;
    border-radius: 15px 0 0 15px;
    right: 0;

    &:hover {
        filter: brightness(1.2);
    }
`;

const PointerContainer = styled.div`
    position: absolute;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    height: 100%;
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-flow: column nowrap;

    > .marker-pointer {
        color: ${props => props.theme.whiteColor};
        font-size: 40px;
        z-index: 999;
    }
`;

const PointerButton = styled.button`
    position: absolute;
    font-size: 21px;
    padding: 16px 24px;
    bottom: 30%;
    color: ${props => props.theme.whiteColor};
    background: ${props => props.theme.redColor};
    box-shadow: ${props => props.theme.boxShadow};
    cursor: pointer;
    z-index: 999;
    border: none;
    border-radius: 15px;

    @media (max-width: 850px) {
        font-size: 16px;
        padding: 12px 16px;
    }

    &:hover {
        filter: brightness(1.5);
    }
`;

const Form = styled.form`
    padding: 16px;
    width: 100%;
    display: flex;
    flex-flow: column nowrap;

    @media (max-width: 850px) {
        width: auto;
    }
`;

const Map = () => {
    const [isLoaded, setIsLoaded] = useState(false);
    const [map, setMap] = useState(null);
    const [showPointer, setShowPointer] = useState(false);
    const [open, setOpen] = useState(false);
    const { latitude, longitude, error } = usePosition(true);
    const place = useSelector(state => state.app.place);
    const dispatch = useDispatch();
    const mapContainer = useRef(null);

    const setNewLocation = place => {
        map.flyTo({
            center: [place.longitude, place.latitude],
            essential: true
        });
    };

    const setNewMarker = event => {
        event.preventDefault();
        const id = uuid();
        const popup = new mapboxgl.Popup({ offset: 25 }).setHTML(
            `<h3>${event.target.spot_name.value}</h3>
                <span>Created by: ${event.target.name.value}</span>
                <br />
                <span>Nearest landmark: ${event.target.landmark.value}</span>
            `
        );

        const el = document.createElement("div");
        el.className = "marker";
        el.id = id;

        // Add onClick listener
        el.addEventListener("click", () => {
            console.log(`${id} Marker Clicked.`);
        });

        const center = map.getCenter();

        // TODO: Save all marker data to Database

        new mapboxgl.Marker(el)
            .setLngLat(center)
            .setPopup(popup)
            .addTo(map);
        setShowPointer(false);
        setOpen(false);
    };

    useEffect(() => {
        setIsLoaded(true);
        dispatch({
            type: "SET_INITIAL_LOCATION",
            response: { latitude, longitude }
        });
    }, [latitude, longitude]);

    useEffect(() => {
        if (place?.latitude && place?.longitude && isLoaded && !map) {
            mapboxgl.accessToken = process.env.REACT_APP_MAPBOX_TOKEN;
            const initializeMap = ({ setMap, mapContainer }) => {
                let map = new mapboxgl.Map({
                    container: mapContainer.current,
                    style: "mapbox://styles/mapbox/dark-v10", // stylesheet location
                    center: [place.longitude, place.latitude],
                    zoom: 16
                });

                map.addControl(
                    new mapboxgl.NavigationControl({
                        showCompass: true,
                        showZoom: true
                    }),
                    "bottom-right"
                );
                map.addControl(
                    new mapboxgl.GeolocateControl({
                        positionOptions: {
                            enableHighAccuracy: true
                        },
                        trackUserLocation: true,
                        showUserLocation: true,
                        showAccuracyCircle: true
                    }),
                    "bottom-right"
                );

                // markerCoords.current = new mapboxgl.Marker()
                //     .setLngLat([place.longitude, place.latitude])
                //     .addTo(map);

                map.on("load", () => {
                    setMap(map);
                    map.resize();
                });
            };

            if (!map) initializeMap({ setMap, mapContainer });
        } else if (
            place?.latitude &&
            place?.longitude &&
            place?.name &&
            isLoaded &&
            map
        ) {
            setNewLocation(place);
        }
    }, [isLoaded, place]);

    return (
        <>
            {error && <h2>{error}</h2>}
            {open && (
                <Modal onClose={() => setOpen(false)}>
                    <Form onSubmit={e => setNewMarker(e)}>
                        <p>Say something about the spot</p>
                        <Input placeholder="Your name" name="name" />
                        <Input placeholder="Landmark" name="landmark" />
                        <Input placeholder="Name the spot" name="spot_name" />
                        <Button type="submit">Submit</Button>
                    </Form>
                </Modal>
            )}
            {!isLoaded ? (
                <MapLoader />
            ) : (
                <Container>
                    {showPointer && (
                        <PointerContainer>
                            <PointerButton onClick={() => setOpen(true)}>
                                Click here to mark smoking area
                            </PointerButton>
                            <MdPlace className="marker-pointer" />
                        </PointerContainer>
                    )}
                    <MapBtnsWrapper>
                        <MapBtn
                            onClick={() => setShowPointer(!showPointer)}
                            title="Show Smoking Area Marker Pointer"
                        >
                            <MdPinDrop />
                        </MapBtn>
                    </MapBtnsWrapper>
                    <MapArea
                        id="mapid"
                        ref={el => (mapContainer.current = el)}
                    />
                </Container>
            )}
        </>
    );
};

export default Map;
