import React, { useState } from "react";
import styled from "styled-components";

const InputBox = styled.input`
    font-size: 14px;
    padding: 12px;
    border: 1px solid ${props => props.theme.grayColor};
    border-radius: 8px;
    color: ${props => props.theme.blackColor};
    margin: 8px 0px;

    @media (max-width: 850px) {
        font-size: 14px;
    }

    &:focus,
    &:hover {
        border: 1px solid ${props => props.theme.orangeColor};
        outline: none;
    }
`;

const Input = ({ placeholder = "", name }) => {
    const [value, setValue] = useState("");
    return (
        <InputBox
            onChange={ev => setValue(ev.target.value)}
            value={value}
            placeholder={placeholder}
            name={name}
        />
    );
};

export default Input;
