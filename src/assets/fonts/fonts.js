import { createGlobalStyle } from "styled-components";

import SmudgeStickTtf from "./SmudgeStick.ttf";
import SmudgeStickEot from "./SmudgeStick.eot";
import SmudgeStickWoff from "./SmudgeStick.woff";

export default createGlobalStyle`
    @font-face {
        font-family: 'Smudgestick';
        src: url(${SmudgeStickTtf});
        src: url(${SmudgeStickEot}) format('embedded-opentype'),
            url(${SmudgeStickWoff}) format('woff'),
            url(${SmudgeStickTtf}) format('truetype');
        font-weight: 400;
        font-style: normal;
    }
`;
