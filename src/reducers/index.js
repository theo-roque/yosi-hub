import { combineReducers } from "redux";
// import storage from "redux-persist/lib/storage";

//  Importing reducers
import app from "../screens/App/App.reducer";

//  Combining all existing reducers
const appReducer = combineReducers({
    app
});

const rootReducer = (state, action) => {
    return appReducer(state, action);
};

export default rootReducer;
