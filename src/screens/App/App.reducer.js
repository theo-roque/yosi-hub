export const actions = {
    SET_INITIAL_LOCATION: "SET_INITIAL_LOCATION",
    SUBMIT_PLACE: "SUBMIT_PLACE",
    SUBMIT_PLACE_FAILED: "SUBMIT_PLACE_FAILED"
};

export const initialState = {
    markers: [],
    loading: false,
    place: null,
    error: null
};

export default (state = initialState, { type, response }) => {
    switch (type) {
        case actions.SET_INITIAL_LOCATION:
            return {
                ...state,
                place: response
            };
        case actions.SUBMIT_PLACE:
            return {
                ...state,
                place: response
            };
        case actions.SUBMIT_PLACE_FAILED:
            return {
                ...state,
                error: response,
                loading: false
            };
        default:
            return state;
    }
};
