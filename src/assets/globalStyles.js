import { createGlobalStyle } from "styled-components";

export const GlobalStyles = createGlobalStyle`
  html, body {
    margin: 0;
    padding: 0;
    font-family: ${props => props.theme.fontPrimary};
    height: 100%;
    width: 100%;
  }
`;
