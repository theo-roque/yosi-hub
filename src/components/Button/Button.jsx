import React from "react";
import styled from "styled-components";

const StyledButton = styled.button`
    font-size: 14px;
    padding: 12px;
    border: none;
    width: 100%;
    border-radius: 8px;
    color: ${props => props.theme.whiteColor};
    background: ${props => props.theme.redColor};
    margin-top: 16px;

    @media (max-width: 850px) {
        font-size: 14px;
    }

    &:hover {
        filter: brightness(1.2);
    }
`;

const Button = ({ children, onClick, type }) => {
    return (
        <StyledButton type={type} onClick={onClick}>
            {children}
        </StyledButton>
    );
};

export default Button;
