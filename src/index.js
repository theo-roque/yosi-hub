import React from "react";
import ReactDOM from "react-dom";
import { ThemeProvider } from "styled-components";
import App from "./screens/App";
import GlobalFonts from "./assets/fonts/fonts";
import { GlobalStyles } from "./assets/globalStyles";
import { theme } from "./assets/theme";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/lib/integration/react";
import { store, persistor } from "./store";
import * as serviceWorker from "./serviceWorker";

ReactDOM.render(
    <PersistGate persistor={persistor}>
        <Provider store={store}>
            <ThemeProvider theme={theme}>
                <GlobalStyles />
                <GlobalFonts />
                <App />
            </ThemeProvider>
        </Provider>
    </PersistGate>,
    document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
