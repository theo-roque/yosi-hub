export const theme = {
    fontPrimary: "Smudgestick",
    whiteColor: "#ffffff",
    blackColor: "#040404",
    grayColor: "#484848",
    lightGrayColor: "#b3b1b1",
    orangeColor: "#ff8100",
    brownColor: "#8e4a03",
    redColor: "#C11B00",
    boxShadow: "0px 5px 5px #00000040",
    textShadow: "0px 2px 2px #00000040",
    closeShadow: "0px 10px 10px #00000029",
    transition: "ease-in-out 300ms"
};
