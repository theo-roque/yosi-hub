import { useState, useEffect, useCallback } from "react";
import axios from "axios";
import { debounce } from "lodash";

export const usePlaceSuggestions = input => {
    const [suggestions, setSuggestions] = useState([]);
    // const autocomplete = useRef();

    // if (!autocomplete.current) {
    //     autocomplete.current
    // }

    function getPlaceSuggestions(input) {
        axios
            .get(
                `https://api.mapbox.com/geocoding/v5/mapbox.places/${input}.json`,
                {
                    params: {
                        access_token: process.env.REACT_APP_MAPBOX_TOKEN
                    }
                }
            )
            .then(res => {
                setSuggestions(
                    res.data.features.map(row => ({
                        name: row.place_name,
                        latitude: row.center[1],
                        longitude: row.center[0]
                    }))
                );
            })
            .catch(error => console.log("error", error));
    }

    const debouncedGetPlacePredictions = useCallback(
        debounce(getPlaceSuggestions, 500),
        []
    );

    useEffect(() => {
        debouncedGetPlacePredictions(input);
    }, [input]);

    return suggestions;
};
