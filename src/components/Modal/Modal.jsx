import React from "react";
import styled from "styled-components";

const Container = styled.div`
    display: flex;
    flex-flow: row nowrap;
    justify-content: center;
    align-items: center;
    background: rgba(100, 100, 100, 0.75);
    position: absolute;
    height: 100%;
    width: auto;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 9999;

    @media (max-width: 850px) {
        padding: 16px;
    }
`;

const Card = styled.div`
    position: relative;
    display: flex;
    flex-flow: column nowrap;
    justify-content: center;
    align-items: center;
    background: ${props => props.theme.whiteColor};
    box-shadow: ${props => props.theme.boxShadow};
    border-radius: 5px;
    padding: 40px;
    text-align: center;
    max-width: 480px;
    width: 100%;

    h1,
    h2,
    h3 {
        color: ${props => props.theme.redColor};
        font-size: 36px;

        @media (max-width: 850px) {
            font-size: 21px;
        }
    }

    p {
        font-family: "Roboto";

        @media (max-width: 850px) {
            font-size: 14px;
        }
    }

    @media (max-width: 850px) {
        width: 100%;
        margin-top: auto;
        margin-bottom: auto;
        padding: 24px 8px;
    }
`;

const BtnClose = styled.button`
    border: 0;
    border-radius: 0 5px 0 5px;
    font-size: 16px;
    font-weight: 700;
    height: 36px;
    width: 36px;
    background: ${props => props.theme.redColor};
    color: ${props => props.theme.whiteColor};
    cursor: pointer;
    position: absolute;
    right: 0;
    top: 0;

    &:hover {
        filter: brightness(1.1);
    }
`;

const Modal = ({ children, onClose }) => {
    return (
        <Container>
            <Card>
                <BtnClose onClick={onClose}>X</BtnClose>
                {children}
            </Card>
        </Container>
    );
};

export default Modal;
